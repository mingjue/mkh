declare module 'virtual:mkh-mod-admin*' {
  import type { MoudleDefinition } from 'mkh-ui'
  const mod: MoudleDefinition
  export default mod
}
